var express = require('express');
var path = require('path');
var _ = require('lodash');
var fetch = require('node-fetch');

var app = express();

// seriously! this should be in an environment variable
// just putting it here to make it easy
var api_key = "7vL0DeiB45Q7bE2k9NZkv6RKJ2XFyGIN";

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'dist')));

let sendIndex = (req, res) => {
  res.sendFile(path.join(__dirname, "index.html"));
}

// define route plans for react app
let reactRoutes = ['/', '/users', '/users/:id'];

_.each(reactRoutes, (route) => {
  app.get(route, sendIndex);
});

// route to return user data from behance
app.get('/behanceUserData', (req, res) => {
  fetch('https://api.behance.net/v2/users?q='+req.query.keyword+'&client_id='+api_key, {
    method: 'GET'
  }).then(response => {
    return response.json();
  },
  error => {
    // don't panic just return null
    return null;
  // continue processing json
  }).then(data => {
    res.send(data);
  });
});

// routed to return user project data from behance
app.get('/behanceUserProjects', (req, res) => {
  fetch('https://api.behance.net/v2/users/'+req.query.user+'/projects?client_id='+api_key, {
    method: 'GET'
  }).then(response => {
    return response.json();
  },
  error => {
    return null;
  }).then(data => {
    res.send(data);
  });
});

// route to return user follow(ers and following) data
app.get('/behanceGetFollow', (req, res) => {
  fetch('https://api.behance.net/v2/users/'+req.query.user+'/'+req.query.field+'?client_id='+api_key, {
    method: 'GET'
  }).then(response => {
    return response.json();
  },
  error => {
    return null;
  }).then(data => {
    res.send(data);
  });
});

app.get('/behanceWorkExperience', (req, res) => {
  fetch('https://www.behance.net/v2/users/'+req.query.user+'/work_experience?client_id='+api_key, {
    method: 'GET'
  }).then(response => {
    return response.json();
  },
  error => {
    return null;
  }).then(data => {
    res.send(data);
  });
});

app.listen(process.env.PORT || 3000, () => {
  console.log("Express server listening on "+ (process.env.PORT || 3000));
});