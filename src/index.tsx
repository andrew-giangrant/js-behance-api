// import application needs here in index.tsx

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, RouteProps } from 'react-router-dom';

import { createStore, Store } from 'redux';
import { UserReducer, UserStore } from './store/redux-reducer';

import 'bootstrap/dist/js/bootstrap.bundle';
import './index.scss';

import { Searcher } from './components/searcher/searcher.component';
import { Profile } from './components/profile/profile.component';

const store: Store<UserStore.All> = createStore(UserReducer, { users: [], searchText: '' });

ReactDOM.render(
  <Router>
    <div className="main-container">
      <div className="main-overlay">
        <Route exact path="/" render={() => <Searcher store={store} />} />
        <Route exact path="/users" render={() => <Searcher store={store} />} />
        <Route path="/users/:id" render={(routeProps:any) => <Profile userId={routeProps.match.params.id}
                                                                      store={store} />} />
      </div>
    </div>
  </Router>,
  document.getElementById("root")
);