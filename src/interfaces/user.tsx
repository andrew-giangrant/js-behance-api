import { UserStats } from './user-stats';

export interface User {
  city: string;
  company: string;
  country: string;
  created_on: number;
  display_name: string;
  fields: string[];
  first_name: string;
  has_default_image: number;
  id: number;
  images: any;
  last_name: string;
  location: string;
  occupation: string;
  state: string;
  stats: UserStats;
  url: string;
  username: string;
  website: string;
}