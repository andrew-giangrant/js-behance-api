export interface Project {
  id: number;
  name: string;
  published_on: number;
  created_on: number;
  modified_on: number;
  url: string;
  fields: string[];
  covers: any;
  mature_content: number;
  owners: any;
  stats: {
    views: number;
    appreciations: number;
    comments: number;
  }
}