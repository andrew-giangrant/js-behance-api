export interface UserStats {
  appreciations: number;
  comments: number;
  followers: number;
  following: number;
  views: number;
}