export interface Experience {
  position: string;
  organization: string;
  location: string;
}