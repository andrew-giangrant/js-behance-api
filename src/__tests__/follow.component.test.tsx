import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as TestUtils from 'react-dom/test-utils';
import * as renderer from 'react-test-renderer';

import { MockFollow } from './mock/mock.follow';
import { MockUser } from './mock/mock.users';

it('should create Follow component successfully', () => {
  const follow = TestUtils.renderIntoDocument(
    <MockFollow user={MockUser} />
  )
  expect(follow).toBeInstanceOf(MockFollow);
});

it('should have user id of 12345', () => {
  const follow = renderer.create(
    <MockFollow user={MockUser} />
  );
  
  expect(follow.root.props.user.id).toBe(12345);
});