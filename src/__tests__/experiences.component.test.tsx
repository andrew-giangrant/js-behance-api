import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as TestUtils from 'react-dom/test-utils';
import * as renderer from 'react-test-renderer';

import { MockExperiences } from './mock/mock.experiences';
import { MockUser } from './mock/mock.users';

it('should create Experiences component successfully', () => {
  // use mock data instead of calling the API
  const experiences = TestUtils.renderIntoDocument(
    <MockExperiences user={MockUser} />
  )
  expect(experiences).toBeInstanceOf(MockExperiences);
});