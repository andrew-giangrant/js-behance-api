import { Projects } from '../../components/profile/projects/projects.component';
import { MockProject } from './mock.project';

// mock projects because we don't want to do an external call
export class MockProjects extends Projects {
  componentDidMount() {
    this.setState({
      projects: [MockProject],
      loading: false
    });
  }
}