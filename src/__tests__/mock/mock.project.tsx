import { Project } from '../../interfaces/project';

export const MockProject: Project = {
  id: 12345,
  name: 'string',
  published_on: 0,
  created_on: 0,
  modified_on: 0,
  url: 'string',
  fields: [],
  covers: {},
  mature_content: 0,
  owners: [],
  stats: {
    views: 0,
    appreciations: 0,
    comments: 0
  }
}