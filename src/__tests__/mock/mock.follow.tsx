import { Follow } from '../../components/profile/follow/follow.component';
import { MockUser } from './mock.users';


// for the same reason we mock Projects we mock Follow
export class MockFollow extends Follow {
  componentDidMount() {
    this.setState({
      followers: [MockUser],
      following: [MockUser],
      loading: false
    });
  }
}