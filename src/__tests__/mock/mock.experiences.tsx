import { Experiences } from '../../components/profile/experience/experience.component';
import { MockExperience } from './mock.experience';


// for the same reason we mock Projects we mock Experiences
export class MockExperiences extends Experiences {
  componentDidMount() {
    this.setState({
      experience: [MockExperience],
      loading: false
    });
  }
}