import { User } from '../../interfaces/user';

// MockUser for testing purposes
export const MockUser: User = {
  id: 12345,
  fields: [],
  state: '',
  stats: {
    followers: 0,
    following: 0,
    views: 0,
    comments: 0,
    appreciations: 0
  },
  city: '',
  company: '',
  country: '',
  created_on: 0,
  display_name: '',
  first_name: '',
  has_default_image: 0,
  images: {},
  last_name: '',
  location: '',
  occupation: '',
  url: '',
  username: '',
  website: ''
}