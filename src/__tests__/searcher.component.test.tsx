import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as TestUtils from 'react-dom/test-utils';

import { Searcher } from '../components/searcher/searcher.component';
import { createStore, Store } from 'redux';
import { UserStore, UserReducer } from '../store/redux-reducer';

const store: Store<UserStore.All> = createStore(UserReducer, { users: [], searchText: '' });

it('should create Searcher component successfully', () => {
  const searcher = TestUtils.renderIntoDocument(
    <Searcher store={store} />
  )
  expect(searcher).toBeInstanceOf(Searcher);
});