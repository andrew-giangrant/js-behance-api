import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as TestUtils from 'react-dom/test-utils';
import * as renderer from 'react-test-renderer';

import { MockProjects } from './mock/mock.projects';
import { MockUser } from './mock/mock.users';

it('should create Projects component successfully', () => {
  // use mock data instead of calling the API
  const projects = TestUtils.renderIntoDocument(
    <MockProjects user={MockUser} />
  )
  expect(projects).toBeInstanceOf(MockProjects);
});

it('should contain one project', () => {
  const projects = renderer.create(
    <MockProjects user={MockUser} />
  );
  
  expect(projects.root.children.length).toBeGreaterThan(0);
  expect(projects.root.children[0]).toBeTruthy();
  
  let child1 = projects.root.find((node:renderer.ReactTestInstance) => {
    return node.children.length === 1;
  });

  expect(child1).toBeDefined();
});