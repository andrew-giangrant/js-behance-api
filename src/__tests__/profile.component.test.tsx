import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as TestUtils from 'react-dom/test-utils';
import { BrowserRouter as Router } from 'react-router-dom';

import { Profile } from '../components/profile/profile.component';
import { createStore, Store } from 'redux';
import { UserStore, UserReducer } from '../store/redux-reducer';

import { MockUser } from './mock/mock.users';

const store: Store<UserStore.All> = createStore(UserReducer, { users: [], searchText: '' });

it('should create Profile component successfully', () => {
  const profile = TestUtils.renderIntoDocument(
    // we need to wrap the Profile in a Router because there is a Redirect component in there
    <Router>
      <Profile userId={MockUser.id.toString()} store={store}/>
    </Router>
  )
  expect(profile).toBeInstanceOf(Router);
});