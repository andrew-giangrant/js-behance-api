import * as React from 'react';
import { User } from '../../../interfaces/user';
import { Experience } from '../../../interfaces/experience';

import './experience.component.scss';

export interface ExperiencesProps {
  user: User;
}

export interface ExperiencesState {
  loading: boolean;
  experience: Experience[];
}

export class Experiences extends React.Component<ExperiencesProps, ExperiencesState> {
  user: User;
  imageKey: string;
  constructor(props:ExperiencesProps) {
    super(props);
    this.state = {
      loading: true,
      experience: []
    }
  }

  componentDidMount() {
    // default experiences to empty array
    var experience:Experience[] = [];
    fetch('/behanceWorkExperience?user='+this.props.user.id, {
      method: 'GET'
    }).then((response:any) => {
      return response.json();
    },
    error => {
      return null;
    }).then((data:any) => {
      console.log(data);
      if(data && data.work_experience && data.work_experience.length) {
        experience = data.work_experience;
      }
      // set state of projects component and stop loading
      // we don't use redux here
      this.setState({
        loading: false,
        experience: experience
      });
    });
  }

  getExperience() {
    // display a spinner while we wait for our behance data to come in
    if(this.state.loading)
      return <div className="loading">
        <span className="fa fa-spinner fa-pulse fa-3x fa-fw" />
      </div>
    // lots of returns here, we return the map ultimately  
    return this.state.experience.map((experience:Experience) => {
      // use our found key for the image and return for our map function
      return <div className="experience">
          <p className="experience-title">{experience.position}</p>
          <p className="experience-company">{experience.organization}</p>
          <p className="experience-location">{experience.location}</p>
        </div>
    });
  }

  render() {
    return <div className="projects-container">
      <h3>Work Experience</h3>
      <div className="projects">
        {this.getExperience()}
      </div>
    </div>
  }
}