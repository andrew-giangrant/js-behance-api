import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { Store } from 'redux';
import { UserStore } from '../../store/redux-reducer';
import { User } from '../../interfaces/user';
import { Projects } from './projects/projects.component';
import { Follow } from './follow/follow.component';
import { Experiences } from './experience/experience.component';

import './profile.component.scss';

export interface ProfileProps {
  userId:string;
  store: Store<UserStore.All>;
}

export interface ProfileState {
  redirect: boolean;
}

export class Profile extends React.Component<ProfileProps, ProfileState> {
  user: User;
  imageKey: string;
  constructor(props:ProfileProps) {
    super(props);
    this.state = {
      redirect: false
    };
    // get user out of the store
    this.user = this.props.store.getState().users.find((user:any) => {
      return user.id === parseInt(this.props.userId);
    });
    if (this.user) {
      this.imageKey = Object.keys(this.user.images).find((key:string) => {
        // look for a image in the 100s since images can be different sizes
        return parseInt(key) > 99 && parseInt(key) < 200;
      })
    }
  }

  // sets a redirect to occur on next render
  setRedirect() {
    this.setState({
      redirect: true
    });
  }

  render() {
    // safely redirect to home if there is no user data available in the store
    // also helps with our rate limiting issue
    if (!this.user || this.state.redirect) return <Redirect to="/" push />
    return <div className="profile-container">
      <div className="back-button" onClick={() => {this.setRedirect()}}>
        <span className="fa fa-arrow-left" onClick={() => {this.setRedirect()}}></span> BACK
      </div>
      <div className="profile-holder">
        <div className="prof-header">
          <img className="profile-pic" src={this.user.images[this.imageKey]} />
          <div className="prof-info">
            <h1>{this.user.display_name}</h1>
            <h4>{this.user.occupation}</h4>
            <h5>{this.user.company}</h5>
            <h6>{this.user.location}</h6>
            {/* created_on time in behance is in seconds times 1000 to milliseconds */}
            <p className="member-since">Member since: {new Date(this.user.created_on*1000).toLocaleDateString()}</p>
          </div>
        </div>
        <div>
          <hr />
        </div>
        <div className="details-container">
          {/* put projects and follow stuff here */}
          <Projects user={this.user} />
          <Experiences user={this.user} />
          <Follow user={this.user} />
        </div>
        <div>
        </div>
      </div>
    </div>
  }
}