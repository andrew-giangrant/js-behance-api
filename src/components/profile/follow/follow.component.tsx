import * as React from 'react';
import { User } from '../../../interfaces/user';

import './follow.component.scss';

export interface FollowProps {
  user: User;
}

export interface FollowState {
  loading: boolean;
  followers: User[];
  following: User[];
  isFollowingOpen: boolean;
}

export class Follow extends React.Component<FollowProps, FollowState> {
  user: User;
  imageKey: string;
  constructor(props:FollowProps) {
    super(props);
    this.state = {
      loading: true,
      followers: [],
      following: [],
      isFollowingOpen: false
    };
  }
  
  componentDidMount() {
    var followers:User[] = [];
    var following:User[] = [];
    
    // callback in this function is getFollowing
    // let's be clear on order of events
    // first fetch followers, then get following
    let getFollowers = (callback:Function) => {
      // url implemented earlier uses field to fetch followers or following
      fetch('/behanceGetFollow?user='+this.props.user.id+'&field=followers', {
        method: 'GET'
      }).then((response:any) => {
        return response.json();
      },
      error => {
        return null;
      }).then((data:any) => {
        if(data && data.followers && data.followers.length) {
          followers = data.followers;
        }
        // basically we move on to getFollowing
        callback();
      });
    };

    // note arrow functions keep us in the this scope
    let getFollowing = () => {
      fetch('/behanceGetFollow?user='+this.props.user.id+'&field=following', {
        method: 'GET'
      }).then((response:any) => {
        return response.json();
      },
      error => {
        return null;
      }).then((data:any) => {
        if(data && data.following && data.following.length) {
          following = data.following;
        }
        // after everything is fetched we can finally set follow data
        // and stop loading
        this.setState({
          loading: false,
          followers: followers,
          following: following
        });
      });
    };

    // avoid callback hell, define functions
    getFollowers(getFollowing);
  }

  // we want to see either followers or following
  setIsFollowingOpen(status:boolean) {
    if(status !== this.state.isFollowingOpen) {
      this.setState({
        isFollowingOpen: status
      });
    }
  }

  getConnections() {
    // nice loading spinner if we don't have the data yet
    // could be it's own component at this point
    if(this.state.loading)
      return <div className="loading">
        <span className="fa fa-spinner fa-pulse fa-3x fa-fw" />
      </div>
    let users = (this.state.isFollowingOpen) ? this.state.following: this.state.followers;
    // we only want an array of the first 20. It's a preview
    return users.slice(0,20).map((user:User) => {
      let imageKey:string = Object.keys(user.images).find((key:string) => {
        // look for a picture of size 100 something
        return parseInt(key) < 200 && parseInt(key) > 99;
      })
      // we return a nice block for each user
      // we'll just link off to behance though we could load them in the profile component
      return <a href={user.url} target="_blank" key={user.id}>
        <div className="user">
          <img className="user-image" src={user.images[imageKey]} />{user.display_name}
        </div>
      </a>;
    });
  }

  render() {
    return <div className="follow-container">
      <div className="controls">
        {/* crude tab system here */}
        <span className={(this.state.isFollowingOpen) ? 'no-clickable': 'clickable'}
                onClick={() => this.setIsFollowingOpen(false)}>
          {this.props.user.stats.followers} Followers
        </span>
        <span className={(this.state.isFollowingOpen) ? 'clickable': 'no-clickable'}
                onClick={() => this.setIsFollowingOpen(true)}>
          {this.props.user.stats.following} Following
        </span>
      </div>
      {/* getConnections is the brain behind users are rendered */}
      <div className="follows">
        {this.getConnections()}
      </div>
    </div>;
  }
}