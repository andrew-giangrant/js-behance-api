import * as React from 'react';
import { User } from '../../../interfaces/user';
import { Project } from '../../../interfaces/project';

import './projects.component.scss';

export interface ProjectsProps {
  user: User;
}

export interface ProjectsState {
  loading: boolean;
  projects: Project[];
}

export class Projects extends React.Component<ProjectsProps, ProjectsState> {
  user: User;
  imageKey: string;
  constructor(props:ProjectsProps) {
    super(props);
    this.state = {
      loading: true,
      projects: []
    }
  }

  componentDidMount() {
    // default projects to empty array
    var projects:Project[] = [];
    fetch('/behanceUserProjects?user='+this.props.user.id, {
      method: 'GET'
    }).then((response:any) => {
      return response.json();
    },
    error => {
      return null;
    }).then((data:any) => {
      if(data && data.projects && data.projects.length) {
        projects = data.projects;
      }
      // set state of projects component and stop loading
      // we don't use redux here
      this.setState({
        loading: false,
        projects: projects
      });
    });
  }

  getProjects() {
    // display a spinner while we wait for our behance data to come in
    if(this.state.loading)
      return <div className="loading">
        <span className="fa fa-spinner fa-pulse fa-3x fa-fw" />
      </div>
    // lots of returns here, we return the map ultimately  
    return this.state.projects.map((project:Project) => {
      let coverKey:string = Object.keys(project.covers).find((key:string) => {
        // find a key in the 100s (size)
        return parseInt(key) < 200 && parseInt(key) > 99;
      })
      // use our found key for the image and return for our map function
      return <a href={project.url} target="_blank" key={project.id}>
        <div className="project">
          <img className="project-image" src={project.covers[coverKey]} />{project.name}
        </div>
      </a>
    });
  }

  render() {
    return <div className="projects-container">
      <h3>Projects</h3>
      <div className="projects">
        {this.getProjects()}
      </div>
    </div>
  }
}