import * as React from 'react';
import { Store, Action } from 'redux';
import { NavLink } from 'react-router-dom';

import './searcher.component.scss';
import { User } from '../../interfaces/user';
import { UserStore, UserReducer } from '../../store/redux-reducer';
import { ActionTypes, TypeKeys, setUsersAction, setSearchAction } from '../../store/action-types';

export interface SearcherState {
  searchVal: string;
  users: User[];
}

export interface SearcherProps {
  store: Store<UserStore.All>
}

export class Searcher extends React.Component<SearcherProps, SearcherState> {
  fetchingUsers:boolean;
  inputElem:HTMLInputElement;
  
  constructor(props:any) {
    super(props);
    this.state = {
      searchVal: '',
      users: []
    };
  }

  componentDidMount() {
    this.inputElem.focus();
  }

  handleSearchInput(evt:any) {
    let keyword = evt.target.value;
    // because Behance rate limits to 150 request per hour
    // if we stopped typing for 800 ms we fetch our data
    setTimeout(() => {
      if(this.state.searchVal === keyword) {
        // use the backend so we don't put an api key in the front end
        fetch('/behanceUserData?keyword='+keyword, {
          method: 'GET'
        }).then((response:Response) => {
          return response.json();
        },
        (error:Response) => {
          // don't panic just return null
          return null;
        // continue processing json
        }).then((data:any) => {
          let users:User[] = [];
          // this will be false if there are no users in results
          // if there is no keyword we don't want to have any results display
          if(data && data.users && data.users.length && keyword) {
            users = data.users;
          }

          // store users and searchVal
          this.props.store.dispatch(setUsersAction(users));
          this.props.store.dispatch(setSearchAction(this.state.searchVal));
          // force update since we are not updating state here
          this.forceUpdate();
        });
      }
    }, 800);
    // we still want to update the input as soon as possible
    // this doesn't belong in our setTimeout function
    this.setState({
      searchVal: keyword
    });
  }

  // gets users for the list
  getResults():any {
    // only return 10 users, that seems like a standard number
    let users = this.props.store.getState().users.slice(0, 10)
    return users.map((user:User) => {
      return <li className="nav-item" key={user.id}>
        <NavLink to={'/users/'+user.id}>{user.display_name}</NavLink>
      </li>
    });
  }

  render() {
    return <div className="searcher-container">
        <div className="search-form-group">
          <span className="fa fa-search search-icon" />
          <input type="search" className="searcher-input" placeholder="Search for user"
                 value={this.state.searchVal}
                 onChange={(evt:any) => this.handleSearchInput(evt)}
                 ref={element => this.inputElem = element} />
        </div>
        <div className="result-list">
          <ul className="nav-item-group">
            {this.getResults()}
          </ul>
        </div>
    </div>;
  }
}