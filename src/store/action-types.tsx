import { User } from '../interfaces/user';

// enums for keys
export enum TypeKeys {
  SET_USERS = "SET_USERS",
  SET_SEARCH_TEXT = "SET_SEARCH_TEXT"
}

//define actions
export function setUsersAction(users:User[]) {
  return {
    type: TypeKeys.SET_USERS,
    users: users
  }
}

export function setSearchAction(searchText:string) {
  return {
    type: TypeKeys.SET_SEARCH_TEXT,
    searchText: searchText
  }
}

export interface setUsersActionType {
  type: TypeKeys.SET_USERS,
  users: User[]
}

export interface setSearchActionType {
  type: TypeKeys.SET_SEARCH_TEXT,
  searchText: string
}


//use a union type so action types can be for users or search input
export type ActionTypes = setUsersActionType | setSearchActionType