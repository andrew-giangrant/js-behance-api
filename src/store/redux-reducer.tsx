import { User } from '../interfaces/user';
import { ActionTypes, TypeKeys } from "./action-types";

// simple reducer for changing state
export function UserReducer(state: any, action: ActionTypes) {
  switch(action.type) {
    case TypeKeys.SET_USERS:
      return Object.assign({}, state, {
        users: action.users
      });
    case TypeKeys.SET_SEARCH_TEXT:
      return Object.assign({}, state, {
        searchText: action.searchText
      });
    default:
      return state;
  }
}

// static types for objects in our store
export namespace UserStore {
  export type UserRedux = User[]

  export type All = {
    users: UserRedux,
    searchText: string
  }
}